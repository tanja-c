/*
  Copyright (c) 2012 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#ifndef _TANJA_H
#define _TANJA_H

#include <inttypes.h>

typedef enum {
	TN_VT_INV = 0,   // invalid type
	TN_VT_INT = 'i', // integer
	TN_VT_NUM = 'n', // number (double, actually)
	TN_VT_STR = 's', // string
	TN_VT_AR  = 'a', // array of tn_el
	TN_VT_MAP = 'm', // map of string -> tn_el
	TN_VT_WC  = '*'  // wildcard
} tn_val_type;


// Recursive typedefs require some ingenuity.
struct tn_el;
typedef struct tn_el tn_el;
struct tn_map_el;
typedef struct tn_map_el tn_map_el;


// Fixed sized struct of 16 bytes. It's much more efficient to embed this
// struct into other types than allocating it separately and storing a pointer
// to it.
// Note that an array or map may not contain more than 65535 elements.
// (It's a bad idea to pass around such large tuples anyway).
struct tn_el {
	uint16_t count; // Number of elements in the array/map
	uint16_t size;  // Number of elements allocated in the array/map
	char     type;  // tn_val_type
	union {
		int64_t   i;
		double    n;
		char      *s; // TODO: Allow statically allocated strings
		tn_el     *a;
		tn_map_el *m;
	} v;
};

struct tn_map_el {
	char  *key; // TODO: Allow statically allocated keys
	tn_el val;
};

typedef struct {
	int   n;   // Number of elements
	int   ref; // Reference count
	tn_el e[1];
} tn_tuple;


// Opaque types
typedef struct tn_returnpath tn_returnpath;
typedef struct tn_node tn_node;
typedef struct tn_session tn_session;
typedef struct tn_link tn_link;

typedef struct {
	void (*dispatch)(tn_link *, void *);
	int (*write)(tn_link *, char *, int, void *);
} tn_link_context;

typedef void (*tn_session_dispatch_cb)(tn_session *, void *);

typedef void (*tn_reply_cb)(tn_session *, tn_tuple *, void *);
typedef void (*tn_tuple_cb)(tn_session *, tn_tuple *, tn_returnpath *, void *);

typedef void (*tn_link_error_cb)(tn_link *, int, char *);
typedef void (*tn_link_ready_cb)(tn_link *);

void tn_el_free(tn_el);
tn_el tn_el_new(char, ...);
tn_el tn_el_copy(tn_el);
int tn_el_intval(tn_el, int64_t *);
int64_t tn_el_int(tn_el);
int tn_el_numval(tn_el, double *);
double tn_el_num(tn_el);
char *tn_el_str(tn_el, char *);
tn_el tn_array_new(const char *, ...);
void tn_array_append(tn_el *, const char *, ...);
tn_el tn_map_new(int, const char *, ...);
void tn_map_set(tn_el *, const char *, ...);
tn_el tn_map_get(tn_el, const const char *);
void tn_tuple_ref(tn_tuple *);
void tn_tuple_unref(tn_tuple *);
tn_tuple *tn_tuple_new(const char *, ...);
int tn_tuple_match(tn_tuple *, tn_tuple *);

char *tn_json_fmt(tn_tuple *);
tn_tuple *tn_json_parse(const char *, int, int *);

void tn_reply(tn_returnpath *, tn_tuple *);
void tn_reply_close(tn_returnpath *rp);

tn_node *tn_node_new();
void tn_node_ref(tn_node *);
void tn_node_unref(tn_node *);

tn_session *tn_session_new(tn_node *, tn_session_dispatch_cb, void *);
void tn_session_ref(tn_session *);
void tn_session_unref(tn_session *);
void tn_session_send(tn_session *, tn_tuple *, tn_reply_cb, void *dat);
int tn_session_register(tn_session *, tn_tuple *, int, tn_tuple_cb, void *);
void tn_session_unregister(tn_session *, int);
int tn_session_dispatch(tn_session *);
void tn_session_close(tn_session *);

tn_link *tn_link_new(tn_node *, tn_link_context *, void *);
void tn_link_ref(tn_link *);
void tn_link_unref(tn_link *);
void tn_link_on_error(tn_link *, tn_link_error_cb);
void tn_link_on_ready(tn_link *, tn_link_ready_cb);
void tn_link_set_sync(tn_link *, int);
int tn_link_startwrite(tn_link *, char **);
void tn_link_endwrite(tn_link *, int);
void tn_link_start(tn_link *);
void tn_link_read(tn_link *, const char *, int);
void tn_link_set_error(tn_link *, int, const char *);
int tn_link_dispatch(tn_link *);
void tn_link_close(tn_link *);

#define tn_el_isvalid(el) ((el).type != TN_VT_INV)

#endif // _TANJA_H

// vim:noet:sw=4:ts=4
