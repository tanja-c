/*
  Copyright (c) 2012 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


// This is a very crude one-thread-per-session "event" model.


#ifndef _TANJA_PTHREAD_H
#define _TANJA_PTHREAD_H

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>

#include <sys/select.h>
#include <pthread.h>
#include <signal.h>

#include "tanja.h"


static inline void _tn_pthread_sig(int x) {
	assert(x == SIGUSR2);
	// No need to do anything, just make sure that any blocking function gets
	// interrupted.
}


// Make sure to handle SIGUSR2. Call this function before creating any sessions
// or nodes. Returns 0 on success, something else otherwise.
static inline int tn_pthread_setsig() {
	struct sigaction a;
	memset(&a, 0, sizeof(a));
	sigemptyset(&a.sa_mask);
	a.sa_flags = 0;
	a.sa_handler = _tn_pthread_sig;
	return sigaction(SIGUSR2, &a, NULL);
}




typedef struct {
	pthread_t self;
	tn_session *s;
} _tn_pthread_s;


static inline void _tn_pthread_sd(tn_session *s, void *_dat) {
	_tn_pthread_s *dat = _dat;
	assert(dat->s == s);
	pthread_kill(dat->self, SIGUSR2);
}


static inline void *_tn_pthread_st(void *_dat) {
	_tn_pthread_s *dat = _dat;
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, PTHREAD_CANCEL_DEFERRED);
	sigset_t mask, oldmask;
	sigemptyset(&mask);
	sigaddset(&mask, SIGUSR2);
	pthread_sigmask(SIG_BLOCK, &mask, &oldmask);
	sigdelset(&oldmask, SIGUSR2);
	while(tn_session_dispatch(dat->s))
		sigsuspend(&oldmask);
	tn_session_unref(dat->s);
	free(dat);
	return NULL;
}


static inline tn_session *tn_session_pthread(tn_node *n, pthread_t *t) {
	_tn_pthread_s *dat = malloc(sizeof(_tn_pthread_s));
	dat->s = tn_session_new(n, _tn_pthread_sd, dat);
	tn_session_ref(dat->s);
	if(pthread_create(&(dat->self), NULL, _tn_pthread_st, dat) < 0) {
		tn_session_unref(dat->s);
		tn_session_close(dat->s);
		return NULL;
	}

	if(t)
		memcpy(t, &(dat->self), sizeof(pthread_t));
	else
		pthread_detach(dat->self);

	return dat->s;
}




#ifndef TN_PTHREAD_READBUFSIZE
# define TN_PTHREAD_READBUFSIZE (10*1024)
#endif


typedef struct {
	pthread_t self;
	tn_link *l;
	int fdin;
	int fdout;
} _tn_pthread_l;


static inline void _tn_pthread_ld(tn_link *l, void *_dat) {
	_tn_pthread_l *dat = _dat;
	assert(l == dat->l);
	pthread_kill(dat->self, SIGUSR2);
}


// Just interrupt whatever is going on, the thread loop will empty the write
// buffer if there's something to write.
static inline int _tn_pthread_ldw(tn_link *l, char *buf, int len, void *_dat) {
	assert(buf && len > 0);
	_tn_pthread_ld(l, _dat);
	return 0;
}


// Returns 0 if there is nothing more to write, 1 otherwise.
static inline int _tn_pthread_lwr(tn_link *l, sigset_t *mask, int fd) {
	char *buf = NULL;
	int len = tn_link_startwrite(l, &buf);
	if(!len)
		return 0;

	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(fd, &fds);
	int n = pselect(fd+1, NULL, &fds, NULL, NULL, mask);
	if(n > 0)
		n = write(fd, buf, len);

	tn_link_endwrite(l, n); // always call _endwrite().

	if(n == 0 || (n < 0 && errno != EINTR))
		tn_link_set_error(l, n==0?1:errno, n == 0 ? "Remote disconnected." : strerror(errno));
	return 1;
}


static inline void _tn_pthread_lrd(tn_link *l, sigset_t *mask, int fd) {
	char buf[TN_PTHREAD_READBUFSIZE];
	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(fd, &fds);
	int n = pselect(fd+1, &fds, NULL, NULL, NULL, mask);
	if(n > 0)
		n = read(fd, buf, TN_PTHREAD_READBUFSIZE);
	if(n < 0 && errno == EINTR)
		return;
	if(n <= 0) // pselect() shouldn't return 0, so that must come from the read()
		tn_link_set_error(l, n == 0 ? 1 : errno, n == 0 ? "Remote disconnected." : strerror(errno));
	else
		tn_link_read(l, buf, n);
}


static inline void *_tn_pthread_lt(void *_dat) {
	_tn_pthread_l *dat = _dat;
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, PTHREAD_CANCEL_DEFERRED);
	sigset_t mask, oldmask;
	sigemptyset(&mask);
	sigaddset(&mask, SIGUSR2);
	pthread_sigmask(SIG_BLOCK, &mask, &oldmask);
	sigdelset(&oldmask, SIGUSR2);
	while(1) {
		do {
			// If there's a callback waiting for the application, dispatch that
			if(!tn_link_dispatch(dat->l))
				goto end;
		} while(_tn_pthread_lwr(dat->l, &oldmask, dat->fdout));

		// If fdlwrite() returned 0, that could also mean that there was an error, check & dispatch.
		if(!tn_link_dispatch(dat->l))
			goto end;

		// Now get to sleep again in a read call.
		_tn_pthread_lrd(dat->l, &oldmask, dat->fdin);
	}
end:
	tn_link_unref(dat->l);
	close(dat->fdout);
	free(dat);
	return NULL;
}


static inline tn_link *tn_link_pthread_fd(tn_node *n, int fdin, int fdout, pthread_t *t) {
	static tn_link_context ctx = {
		_tn_pthread_ld,
		_tn_pthread_ldw
	};

	_tn_pthread_l *dat = malloc(sizeof(_tn_pthread_l));
	dat->fdin = fdin;
	dat->fdout = fdout;
	dat->l = tn_link_new(n, &ctx, dat);
	tn_link_ref(dat->l);
	if(pthread_create(&dat->self, NULL, _tn_pthread_lt, dat) < 0) {
		tn_link_unref(dat->l);
		tn_link_close(dat->l);
		return NULL;
	}

	if(t)
		memcpy(t, &dat->self, sizeof(pthread_t));
	else
		pthread_detach(dat->self);

	return dat->l;
}


#endif // _TANJA_PTHREAD_H

// vim:noet:sw=4:ts=4
