/*
  Copyright (c) 2012 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#include <stdlib.h>
#include <stddef.h>
#include <stdarg.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>

#include "khash.h"
#include "tanja.h"

// Maximum nesting level of JSON arrays/objects
#ifndef TANJA_JSON_MAXNEST
# define TANJA_JSON_MAXNEST 50
#endif

// TODO: Provide pthread-like fallback for the atomic operation?
#ifdef TANJA_THREADSAFE
# include <pthread.h>
# define atomic_inc(n)    __sync_add_and_fetch(&(n), 1)
# define atomic_dec(n)    __sync_add_and_fetch(&(n), -1)
# define mutex_t(n)       pthread_mutex_t n
# define mutex_init(l)    pthread_mutex_init(&(l), NULL)
# define mutex_lock(l)    pthread_mutex_lock(&(l))
# define mutex_unlock(l)  pthread_mutex_unlock(&(l))
# define mutex_free(l)    pthread_mutex_destroy(&(l))
#else
# define atomic_inc(n) ++(n)
# define atomic_dec(n) --(n)
# define mutex_t(n)
# define mutex_init(l)
# define mutex_lock(l)
# define mutex_unlock(l)
# define mutex_free(l)
#endif




// Forward declarations to make things work
typedef struct patternreg patternreg;
static void tn_session_recv(tn_session *, tn_tuple *, patternreg *, tn_returnpath *);
static void tn_session_reply(tn_session *, tn_tuple *, tn_returnpath *);
static void tn_link_node_reg(tn_link *, int, patternreg *);
static void tn_link_node_unreg(tn_link *, int, patternreg *);
static void tn_link_node_recv(tn_link *, tn_tuple *, tn_returnpath *);
static void tn_link_reply(tn_link *, tn_tuple *, tn_returnpath *);



// Map of integers to patternreg pointers
KHASH_INIT(pr, int, patternreg*, 1, kh_int_hash_func2, kh_int_hash_equal)

// Map of integers to returnpath pointers
KHASH_INIT(rp, int, tn_returnpath*, 1, kh_int_hash_func2, kh_int_hash_equal)

// Map of integers to integers
KHASH_INIT(ii, int, int, 1, kh_int_hash_func2, kh_int_hash_equal)

// Set of tn_link pointers
#define ln_hash_f(p) kh_int64_hash_func((khint64_t)p)
#define ln_hash_eq(a,b) ((a) == (b))
KHASH_INIT(ln, tn_link *, char, 0, ln_hash_f, ln_hash_eq)
#undef ln_hash_f
#undef ln_hash_eq



// Tiny buffer abstraction for internal use
// Note that these macros aren't very safe with respect to argument naming /
// evaluation. Only pass variables directly.

typedef struct {
	char *dat;
	int len;
	int size;
} lbuf;

#define lbuf_init(b) do {\
		b.dat = NULL;\
		b.size = b.len = 0;\
	} while(0)

#define lbuf_free(b) do {\
		if(b.dat)\
			free(b.dat);\
		lbuf_init(b);\
	} while(0)

#define lbuf_grow(b, _m) do {\
		if(b.size < 16)\
			b.size = 16;\
		else\
			b.size *= 2;\
		if(b.size < _m)\
			b.size = _m;\
		b.dat = realloc(b.dat, b.size);\
	} while(0)

#define lbuf_append(b, _buf, _len) do {\
		if(b.size < b.len + _len)\
			lbuf_grow(b, b.len + _len);\
		memcpy(b.dat+b.len, _buf, _len);\
		b.len += _len;\
	} while(0)

#define lbuf_append_c(b, _c) do {\
		if(b.size < b.len + 1)\
			lbuf_grow(b, b.len + 1);\
		b.dat[b.len++] = _c;\
	} while(0)

#define lbuf_shift(b, _len) do {\
		memmove(b.dat, b.dat+_len, b.len-_len);\
		b.len -= _len;\
	} while(0)






// Generic tuple management


// Only frees the content, not the element itself. (Obviously, it's not even given a pointer)
void tn_el_free(tn_el el) {
	int i;
	switch(el.type) {
	case TN_VT_STR:
		free(el.v.s);
		break;
	case TN_VT_AR:
		for(i=0; i<el.count; i++)
			tn_el_free(el.v.a[i]);
		free(el.v.a);
		break;
	case TN_VT_MAP:
		for(i=0; i<el.size; i++) {
			if(el.v.m[i].key) {
				free(el.v.m[i].key);
				tn_el_free(el.v.m[i].val);
			}
		}
		free(el.v.m);
		break;
	}
}


static inline tn_el el_newv(char t, va_list *va) {
	if(t == TN_VT_AR || t == TN_VT_MAP)
		return va_arg(*va, tn_el);
	tn_el a;
	a.type = t;
	switch(t) {
	case TN_VT_INT: a.v.i = va_arg(*va, int64_t); break;
	case TN_VT_NUM: a.v.n = va_arg(*va, double); break;
	case TN_VT_STR: a.v.s = va_arg(*va, char *); break;
	default: assert(t == TN_VT_WC);
	}
	return a;
}


tn_el tn_el_new(char t, ...) {
	va_list va;
	va_start(va, t);
	tn_el e = el_newv(t, &va);
	va_end(va);
	return e;
}


tn_el tn_el_copy(tn_el el) {
	int i;
	tn_el r = el;
	switch(el.type) {
	case TN_VT_STR:
		r.v.s = el.v.s ? strdup(el.v.s) : NULL;
		break;
	case TN_VT_AR:
		r.v.a = malloc(el.count*sizeof(tn_el));
		for(i=0; i<el.count; i++)
			r.v.a[i] = tn_el_copy(el.v.a[i]);
		break;
	case TN_VT_MAP:
		if(el.v.m)
			r.v.m = malloc(el.size*sizeof(tn_map_el));
		for(i=0; i<el.size; i++) {
			if(el.v.m[i].key) {
				r.v.m[i].key = strdup(el.v.m[i].key);
				r.v.m[i].val = tn_el_copy(el.v.m[i].val);
			}
		}
		break;
	}
	return r;
}


int tn_el_intval(tn_el el, int64_t *val) {
	switch(el.type) {
	case TN_VT_INT:
		if(val)
			*val = el.v.i;
		return 1;
	case TN_VT_NUM:
		if(el.v.n > INT64_MAX || el.v.n < INT64_MIN)
			return 0;
		if(val)
			*val = el.v.n;
		return 1;
	case TN_VT_STR:;
		char *e = NULL;
		int64_t v = strtoll(el.v.s, &e, 10);
		if(!e || e == el.v.s || *e) {
			double d;
			int l;
			if(sscanf(el.v.s, "%lf%n", &d, &l) < 1 || l != (int)strlen(el.v.s) || d > INT64_MAX || d < INT64_MIN)
				return 0;
			if(val)
				*val = d;
		} else if(val)
			*val = v;
		return 1;
	}
	return 0;
}


int64_t tn_el_int(tn_el el) {
	int64_t r = 0;
	tn_el_intval(el, &r);
	return r;
}


int tn_el_numval(tn_el el, double *val) {
	switch(el.type) {
	case TN_VT_INT:
		if(val)
			*val = el.v.i;
		return 1;
	case TN_VT_NUM:
		if(val)
			*val = el.v.n;
		return 1;
	case TN_VT_STR:;
		double v;
		int l;
		if(sscanf(el.v.s, "%lf%n", &v, &l) < 1 || l != (int)strlen(el.v.s))
			return 0;
		if(val)
			*val = v;
		return 1;
	}
	return 0;
}


double tn_el_num(tn_el el) {
	double r = 0.0;
	tn_el_numval(el, &r);
	return r;
}


// buf, if set, must be at least 25 bytes long.
char *tn_el_str(tn_el el, char *buf) {
	switch(el.type) {
	case TN_VT_INT:
		if(buf)
			snprintf(buf, 25, "%"PRId64, el.v.i);
		return buf;
	case TN_VT_NUM:
		if(buf)
			snprintf(buf, 25, "%.15g", el.v.n);
		return buf;
	case TN_VT_STR:;
		return el.v.s;
	}
	return NULL;
}


tn_el tn_array_new(const char *lst, ...) {
	tn_el a;
	a.count = a.size = strlen(lst);
	a.type = TN_VT_AR;
	va_list va;
	va_start(va, lst);
	if(a.count) {
		a.v.a = malloc(a.size*sizeof(tn_el));
		int i;
		for(i=0; i<a.count; i++)
			a.v.a[i] = el_newv(lst[i], &va);
	} else
		a.v.a = NULL;
	va_end(va);
	return a;
}


static inline void tn_array_grow(tn_el *a, int new) {
	if(a->count+new > a->size) {
		a->size *= 2;
		if(a->size < new+a->count)
			a->size = new+a->count;
		a->v.a = realloc(a->v.a, a->size*sizeof(tn_el));
	}
}


void tn_array_append(tn_el *a, const char *lst, ...) {
	assert(a && a->type == TN_VT_AR);

	int n = strlen(lst);
	tn_array_grow(a, n);

	va_list va;
	va_start(va, lst);
	int i;
	for(i=0; i<n; i++)
		a->v.a[a->count++] = el_newv(lst[i], &va);
	va_end(va);
}


// A map is implemented as a simple hash table using open addressing, linear
// probing and grows as soon as the table is 75% full. (3/4 is easy for integer
// calculation). Uses a string hash function provided by khash.h and
// power-of-two table sizes (it is assumes that the hash function is random
// enough). Uses key=NULL to indicate unused buckets. Deletion is not
// supported, so that simplifies things a bit.

// Get the index of item with the given key. If the returned index' .key =
// NULL, then no such item exists and the index is the location where it should
// be inserted. Assumes the table is not full.
static inline int tn_map_getidx(tn_el m, const char *key) {
	int i = kh_str_hash_func(key) & (m.size-1);
	while(m.v.m[i].key && strcmp(key, m.v.m[i].key) != 0)
		i = (i+1) & (m.size-1);
	return i;
}


static inline void tn_map_grow(tn_el *m, int new) {
	if(!m->count && !new)
		return;
	int s = m->size;
	if(!s)
		s = 16;
	while(s < ((m->count+new)*4)/3)
		s <<= 1;
	assert(s < UINT16_MAX); // Might want to handle this
	if(s == m->size)
		return;

	// Now we'll have to grow. This is a simple yet not-very-space-efficient solution.
	tn_el n = *m;
	n.size = s;
	n.v.m = calloc(n.size, sizeof(tn_map_el));
	int i;
	for(i=0; i<m->size; i++)
		if(m->v.m[i].key) {
			int idx = tn_map_getidx(n, m->v.m[i].key);
			n.v.m[idx].key = m->v.m[i].key;
			n.v.m[idx].val = m->v.m[i].val;
		}
	free(m->v.m);
	*m = n;
}


// Frees and overwrites an previous item if there already was one with the same key.
static inline void tn_map_set_one(tn_el *m, char *key, tn_el val) {
	int i = tn_map_getidx(*m, key);
	if(m->v.m[i].key) {
		free(m->v.m[i].key);
		tn_el_free(m->v.m[i].val);
	} else
		m->count++;
	m->v.m[i].key = key;
	m->v.m[i].val = val;
}


tn_el tn_map_new(int s, const char *lst, ...) {
	tn_el a;
	a.type = TN_VT_MAP;
	a.v.m = NULL;
	a.size = a.count = 0;

	int len = strlen(lst);
	if(s < len)
		s = len;
	tn_map_grow(&a, s);

	va_list va;
	va_start(va, lst);
	int i;
	for(i=0; i<len; i++) {
		char *k = va_arg(va, char *);
		tn_map_set_one(&a, k, el_newv(lst[i], &va));
	}
	va_end(va);
	return a;
}


void tn_map_set(tn_el *m, const char *lst, ...) {
	assert(m && m->type == TN_VT_MAP);
	int len = strlen(lst);
	tn_map_grow(m, len);
	va_list va;
	va_start(va, lst);
	int i;
	for(i=0; i<len; i++) {
		char *k = va_arg(va, char *);
		tn_map_set_one(m, k, el_newv(lst[i], &va));
	}
	va_end(va);
}


tn_el tn_map_get(tn_el m, const char *key) {
	assert(m.type == TN_VT_MAP);
	int i = tn_map_getidx(m, key);
	if(m.v.m[i].key)
		return m.v.m[i].val;
	tn_el r;
	r.type = TN_VT_INV;
	return r;
}


void tn_tuple_ref(tn_tuple *tup) {
	assert(tup != NULL);
	atomic_inc(tup->ref);
}


void tn_tuple_unref(tn_tuple *tup) {
	assert(tup != NULL);
	if(!atomic_dec(tup->ref)) {
		int i;
		for(i=0; i<tup->n; i++)
			tn_el_free(tup->e[i]);
		free(tup);
	}
}


tn_tuple *tn_tuple_new(const char *lst, ...) {
	tn_tuple *t = malloc(offsetof(tn_tuple, e) + strlen(lst)*sizeof(tn_el));
	t->n = strlen(lst);
	t->ref = 1;
	va_list va;
	va_start(va, lst);
	int i;
	for(i=0; i<t->n; i++)
		t->e[i] = el_newv(lst[i], &va);
	va_end(va);
	return t;
}


int tn_tuple_match(tn_tuple *p, tn_tuple *t) {
	if(p->n > t->n)
		return 0;
	int i;
	for(i=0; i<p->n; i++) {
		tn_el *ep = p->e+i;
		tn_el *et = t->e+i;
		// Wildcards always match
		if(ep->type == TN_VT_WC || et->type == TN_VT_WC)
			continue;
		// Maps and arrays can't be matched on
		if(et->type == TN_VT_MAP || et->type == TN_VT_AR)
			return 0;
		// Simple string comparison
		if(et->type == TN_VT_STR && ep->type == TN_VT_STR && strcmp(et->v.s, ep->v.s) == 0)
			continue;
		// What's left is integer comparison, which isn't very obvious.
		int64_t ip = 0, it = 0;
		if(!tn_el_intval(*ep, &ip) || !tn_el_intval(*et, &it) || ip != it)
			return 0;
	}
	return 1;
}






// Tuple-to-json encoder


#define ac(_c) lbuf_append_c((*buf), _c)

#define as(_s) do {\
		int _len = strlen(_s);\
		lbuf_append((*buf), _s, _len);\
	} while(0)

static inline void json_fmt_string(const char *str, lbuf *buf) {
	ac('\"');
	while(*str) {
		switch(*str) {
		case '\n': ac('\\'); ac('n'); break;
		case '\r': ac('\\'); ac('r'); break;
		case '\b': ac('\\'); ac('b'); break;
		case '\t': ac('\\'); ac('t'); break;
		case '\f': ac('\\'); ac('f'); break;
		case '\\': ac('\\'); ac('\\'); break;
		case '"':  ac('\\'); ac('"'); break;
		default:
			if((unsigned char)*str <= 31 || (unsigned char)*str == 127) {
				char b[8] = {};
				snprintf(b, 8, "\\u00%02x", *str);
				as(b);
			} else
				ac(*str);
		}
		str++;
	}
	ac('\"');
}


static void json_fmt_el(tn_el e, lbuf *buf) {
	char b[25];
	int i;
	switch(e.type) {
	case TN_VT_INT:
		snprintf(b, 25, "%"PRId64, e.v.i);
		as(b);
		break;
	case TN_VT_NUM:
		snprintf(b, 25, "%.15g", e.v.n);
		as(b);
		break;
	case TN_VT_STR:
		json_fmt_string(e.v.s, buf);
		break;
	case TN_VT_AR:
		ac('[');
		for(i=0; i<e.count; i++) {
			if(i)
				ac(',');
			json_fmt_el(e.v.a[i], buf);
		}
		ac(']');
		break;
	case TN_VT_MAP:
		ac('{');
		int p = 0;
		for(i=0; i<e.size; i++) {
			if(e.v.m[i].key) {
				if(p++)
					ac(',');
				json_fmt_string(e.v.m[i].key, buf);
				ac(':');
				json_fmt_el(e.v.m[i].val, buf);
			}
		}
		ac('}');
		break;
	default:
		assert(e.type == TN_VT_WC);
		as("null");
	}
}


static void tn_json_fmt_buf(tn_tuple *tup, lbuf *buf) {
	ac('[');
	int i;
	for(i=0; i<tup->n; i++) {
		if(i)
			ac(',');
		json_fmt_el(tup->e[i], buf);
	}
	ac(']');
}

#undef as
#undef ac


// Returns a (0-terminated) JSON representation of the tuple. The returned
// buffer must be free()'d after use.
char *tn_json_fmt(tn_tuple *tup) {
	lbuf buf;
	lbuf_init(buf);
	tn_json_fmt_buf(tup, &buf);
	lbuf_append_c(buf, 0);
	return buf.dat;
}






// Json-to-tuple parser
// Error handling should be correct, but informative error messages are completely absent.
// TODO: validate that it's correct UTF-8?


#define con(n) do { *buf += n; *len -= n; } while(0)
#define cons() do { while(*len > 0 && (**buf == 0x20 || **buf == 0x09 || **buf == 0x0A || **buf == 0x0D)) { con(1); } } while(0)

static tn_el tn_json_invalid; // global vars are initialized to zero by default

static inline tn_el tn_json_parse_val(char **buf, int *len, int lvl);


static int tn_json_parse_string_esc(char **buf, int *len, lbuf *b) {
#define ap(c) do { lbuf_append_c((*b), c); con(1); } while(0)
	switch(**buf) {
	case '"':  ap('"');  break;
	case '\\': ap('\\'); break;
	case '/':  ap('/');  break;
	case 'b':  ap(0x08); break;
	case 'f':  ap(0x0C); break;
	case 'n':  ap(0x0A); break;
	case 'r':  ap(0x0D); break;
	case 't':  ap(0x09); break;
	case 'u':
		if(*len < 5)
			return 0;
#define hn(n) (n >= '0' && n <= '9' ? n-'0' : n >= 'A' && n <= 'F' ? n-'A'+10 : n >= 'a' && n <= 'f' ? n-'a'+10 : 1<<16)
		uint32_t n = (hn((*buf)[1])<<12) + (hn((*buf)[2])<<8) + (hn((*buf)[3])<<4) + hn((*buf)[4]);
#undef hn
		if(n <= 0x007F)
			lbuf_append_c((*b), n);
		else if(n <= 0x07FF) {
			lbuf_append_c((*b), 0xC0 | (n>>6));
			lbuf_append_c((*b), 0x80 | (n & 0x3F));
		} else if(n <= 0xFFFF) {
			lbuf_append_c((*b), 0xE0 | (n>>12));
			lbuf_append_c((*b), 0x80 | ((n>>6) & 0x3F));
			lbuf_append_c((*b), 0x80 | (n & 0x3F));
		} else // this happens if there was an invalid character (n >= (1<<16))
			return 0;
		con(5);
		break;
	default:
		return 0;
	}
#undef ap
	return 1;
}


static tn_el tn_json_parse_string(char **buf, int *len) {
	con(1); // consume " character
	lbuf b;
	lbuf_init(b);
	while(1) {
		if(!*len)
			goto err;
		// Complete string, return
		if(**buf == '"') {
			con(1);
			lbuf_append_c(b, 0);
			tn_el el;
			el.type = TN_VT_STR;
			el.v.s = b.dat;
			return el;
		}
		// Control characters are not allowed
		if((unsigned char)**buf <= 0x1F || (unsigned char)**buf == 0x7F)
			goto err;
		// backspace
		if(**buf == '\\') {
			con(1);
			if(!*len || !tn_json_parse_string_esc(buf, len, &b))
				goto err;
		} else {
			// Normal character
			lbuf_append_c(b, **buf);
			con(1);
		}
	}
err:
	lbuf_free(b);
	return tn_json_invalid;
}


// flags:
// 	 1: allow a leading zero. (i.e. !1 allows '0' but not '0xxx')
// 	 2: allow a leading '-' sign
// 	 4: allow a leading '+' sign
// Result is stored in *res, number of digits used in *dig, number of (least
// significant) digits ignored in *ign. (So actual value is in the order of
// res*(10^ign)). Returns 0 on error, 1 otherwise.
static inline int tn_json_parse_int(char **buf, int *len, int flags, int64_t *res, int *dig, int *ign) {
	// Special-case -2^63, it's not handled by the code below. (And luckily it
	// only has a single representation in JSON. Unless allowzero is true...
	// but you don't want -2^63 as an exponent and allowsign is disabled for
	// the fractional part.)
	if((flags & 2) && *len >= 20 && strncmp(*buf, "-9223372036854775808", 20) == 0) {
		con(20);
		*res = INT64_MIN;
		*dig = 19;
		*ign = 0;
		return 1;
	}
	*res = 0;
	*ign = *dig = 0;
	int hassign = 0; // 1 == -, 2 == +
	int haszero = 0;
	while(1) {
		if(!*len)
			break;
		if(**buf == '+') {
			if(hassign || *dig || !(flags & 4))
				break;
			hassign = 2;
		} else if(**buf == '-') {
			if(hassign || *dig || !(flags & 2))
				break;
			hassign = 1;
		} else if(**buf >= '0' && **buf <= '9') {
			if(haszero && !(flags & 1))
				break;
			if(!*dig && **buf == '0')
				haszero = 1;
			if(*ign)
				(*ign)++;
			else {
				int64_t n = ((*res)*10) + **buf-'0';
				if(n < *res) // overflow
					(*ign)++;
				else {
					(*dig)++;
					*res = n;
				}
			}
		} else
			break;
		con(1);
	}
	if(hassign == 1)
		*res = -1*(*res);
	return *dig > 0;
}


static tn_el tn_json_parse_num(char **buf, int *len) {
	int64_t i, d = 0, e = 0;
	int id, ii, dd = 0, di, ed, ei, neg = 0;
	// Integer part
	// Explicitely check for the '-' to determine whether the number is
	// negative or not, can't depend on 'i' since that may be 0. (e.g. -0.123
	// is definitely negative).
	if(*len && **buf == '-')
		neg = 1;
	if(!tn_json_parse_int(buf, len, 2, &i, &id, &ii))
		return tn_json_invalid;
	// Decimal
	if(len && **buf == '.') {
		con(1);
		if(!tn_json_parse_int(buf, len, 1, &d, &dd, &di))
			return tn_json_invalid;
	}
	// Exponent
	if(len && (**buf == 'e' || **buf == 'E')) {
		con(1);
		if(!tn_json_parse_int(buf, len, 1|2|4, &e, &ed, &ei))
			return tn_json_invalid;
	}
	// Create element
	tn_el el;
	if(!d && !e && !ii) { // fits into a regular int64
		el.type = TN_VT_INT;
		el.v.i = i;
	} else {
		el.type = TN_VT_NUM;
		el.v.n = (double)i;
		// Can these operations be done more efficiently by playing around with the bits instead of using pow()?
		if(e || ii) { // exponent
			el.v.n *= pow(10.0, (double)(e + ii));
			if(el.v.n == HUGE_VAL)
				return tn_json_invalid;
		}
		if(d && !ii) // decimal (don't bother if ii>0)
			el.v.n += (double)(neg ? -d : d) * pow(10.0, (double)(e - dd));
	}
	return el;
}


static tn_el tn_json_parse_array(char **buf, int *len, int lvl) {
	con(1); // consume '[' character
	tn_el el = tn_array_new("");
	int first = 1;
	while(1) {
		cons();
		if(!*len)
			goto err;
		// end of array, return.
		if(**buf == ']') {
			con(1);
			return el;
		}
		// consume comma
		if(first)
			first = 0;
		else {
			if(**buf != ',')
				goto err;
			con(1);
			cons();
			if(!*len)
				goto err;
		}
		// get next value
		tn_array_grow(&el, 1);
		el.v.a[el.count] = tn_json_parse_val(buf, len, lvl);
		if(!el.v.a[el.count].type)
			goto err;
		el.count++;
	}
err:
	tn_el_free(el);
	return tn_json_invalid;
}


static tn_el tn_json_parse_map(char **buf, int *len, int lvl) {
	con(1); // consume '{' character
	tn_el el = tn_map_new(0, "");
	int first = 1;
	while(1) {
		cons();
		if(!*len)
			goto err;
		// end of map, return.
		if(**buf == '}') {
			con(1);
			return el;
		}
		// consume comma
		if(first)
			first = 0;
		else {
			if(**buf != ',')
				goto err;
			con(1);
			cons();
			if(!*len)
				goto err;
		}
		// get key
		tn_map_grow(&el, 1);
		if(**buf != '"')
			goto err;
		tn_el key = tn_json_parse_string(buf, len);
		if(!key.type)
			goto err;
		// consume separator
		cons();
		if(!*len || **buf != ':') {
			free(key.v.s);
			goto err;
		}
		con(1);
		cons();
		if(!*len) {
			free(key.v.s);
			goto err;
		}
		// get value
		tn_el val = tn_json_parse_val(buf, len, lvl);
		if(!val.type) {
			free(key.v.s);
			goto err;
		}
		tn_map_grow(&el, 1);
		tn_map_set_one(&el, key.v.s, val);
	}
err:
	tn_el_free(el);
	return tn_json_invalid;
}


static inline tn_el tn_json_parse_val(char **buf, int *len, int lvl) {
	if(--lvl < 1)
		return tn_json_invalid;
	if(*len >= 2 && **buf == '[')
		return tn_json_parse_array(buf, len, lvl);
	else if(*len >= 2 && **buf == '"')
		return tn_json_parse_string(buf, len);
	else if(*len >= 2 && **buf == '{')
		return tn_json_parse_map(buf, len, lvl);
	else if(*len >= 1 && (**buf == '-' || (**buf >= '0' && **buf <= '9')))
		return tn_json_parse_num(buf, len);
	else if(*len >= 4 && strncmp(*buf, "true", 4) == 0) {
		con(4);
		tn_el el;
		el.type = TN_VT_INT;
		el.v.i = 1;
		return el;
	} else if(*len >= 5 && strncmp(*buf, "false", 5) == 0) {
		con(5);
		tn_el el;
		el.type = TN_VT_INT;
		el.v.i = 0;
		return el;
	} else if(*len >= 4 && strncmp(*buf, "null", 4) == 0) {
		con(4);
		tn_el el;
		el.type = TN_VT_WC;
		return el;
	}
	return tn_json_invalid;
}


// Returns NULL on error. If not NULL, *rd will indicate the number of bytes
// that have been read from the buffer. If there was an error, this points to
// the problem.
tn_tuple *tn_json_parse(const char *_buf, int _len, int *rd) {
	int l = _len;
	int *len = &l;
	char *b = (char *)_buf;
	char **buf = &b;
	cons();
	if(*len < 2 || **buf != '[')
		return NULL;
	tn_el el = tn_json_parse_array(buf, len, TANJA_JSON_MAXNEST-1);
	if(rd)
		*rd =  _len - l;
	// Convert array into tuple
	if(el.type) {
		tn_tuple *tup = malloc(offsetof(tn_tuple, e) + el.count*sizeof(tn_el));
		tup->n = el.count;
		tup->ref = 1;
		memcpy(&tup->e, el.v.a, el.count*sizeof(tn_el));
		if(el.size)
			free(el.v.a);
		return tup;
	}
	return NULL;
}

#undef con
#undef cons






// Return-path

struct tn_returnpath {
	int ref; // Number of sessions sending to it. May be 0.
	int type; // 0 = session, 1 = link
	union {
		struct {
			tn_session *s;
			tn_reply_cb cb;
			void *data;
		} s;
		struct {
			tn_link *l;
			int id;
		} l;
	} t;
};


void tn_reply(tn_returnpath *rp, tn_tuple *t) {
	assert(t);
	if(!rp) {
		tn_tuple_unref(t);
		return;
	} else if(rp->type == 0)
		tn_session_reply(rp->t.s.s, t, rp);
	else
		tn_link_reply(rp->t.l.l, t, rp);
}


void tn_reply_close(tn_returnpath *rp) {
	if(rp && !atomic_dec(rp->ref)) {
		if(rp->type == 0)
			tn_session_reply(rp->t.s.s, NULL, rp);
		else
			tn_link_reply(rp->t.l.l, NULL, rp);
	}
}


static inline void tn_returnpath_open(tn_returnpath *rp) {
	atomic_inc(rp->ref);
}


static inline tn_returnpath *tn_returnpath_new_ses(tn_session *ses, tn_reply_cb cb, void *dat) {
	tn_returnpath *rp = malloc(sizeof(tn_returnpath));
	rp->ref = 0; // No senders yet
	rp->type = 0;
	rp->t.s.s = ses;
	rp->t.s.cb = cb;
	rp->t.s.data = dat;
	tn_session_ref(ses);
	return rp;
}


static inline tn_returnpath *tn_returnpath_new_link(tn_link *l, int tid) {
	tn_returnpath *rp = malloc(sizeof(tn_returnpath));
	rp->ref = 0; // No senders yet
	rp->type = 1;
	rp->t.l.l = l;
	rp->t.l.id = tid;
	tn_link_ref(l);
	return rp;
}


// Called after the 'path closed' notification has been dispatched/handled.
static inline void tn_returnpath_free(tn_returnpath *rp) {
	if(!rp->type)
		tn_session_unref(rp->t.s.s);
	else
		tn_link_unref(rp->t.l.l);
	free(rp);
}






// A registered pattern

struct patternreg {
	tn_tuple *pat; // Only accessed by the node, only valid if active=1
	int active;    // Atomic int, whether the pattern is still registered or not
	int ref;
	int willreply; // Always 1 for links
	int type;      // 0 = session, 1 = link
	void *recipient; // either a tn_session or tn_link pointer
	// For sessions, links don't need any additional data
	tn_tuple_cb cb;
	void *data;
};


static inline patternreg *patternreg_new_ses(tn_tuple *pat, int willreply, tn_session *ses, tn_tuple_cb cb, void *dat) {
	patternreg *r = malloc(sizeof(patternreg));
	r->ref = 1;
	r->active = 1;
	r->type = 0;
	r->pat = pat;
	r->willreply = willreply;
	r->recipient = ses; // No need to ref() this, the pattern is unregistered when the session is closed anyway
	r->cb = cb;
	r->data = dat;
	return r;
}


static inline patternreg *patternreg_new_link(tn_tuple *pat, tn_link *l) {
	patternreg *r = malloc(sizeof(patternreg));
	r->ref = 1;
	r->active = 1;
	r->type = 1;
	r->pat = pat;
	r->willreply = 1;
	r->recipient = l;
	return r;
}


static inline void patternreg_ref(patternreg *p) {
	atomic_inc(p->ref);
}


static inline void patternreg_unref(patternreg *p) {
	if(!atomic_dec(p->ref)) {
		assert(!p->active);
		free(p);
	}
}





// Node


// TODO: Organize the patterns in a tree of hash tables to speed up matching?
// Note: Links access some of these fields (and the lock) directly.
struct tn_node {
	mutex_t(lock);
	khash_t(pr) *regs;
	khash_t(ln) *links;
	int lastid;
	int ref;
};


tn_node *tn_node_new() {
	tn_node *n = malloc(sizeof(tn_node));
	mutex_init(n->lock);
	n->regs = kh_init(pr);
	n->links = kh_init(ln);
	n->ref = 1;
	n->lastid = 0;
	return n;
}

void tn_node_ref(tn_node *n) {
	assert(n != NULL);
	atomic_inc(n->ref);
}


void tn_node_unref(tn_node *n) {
	assert(n != NULL);
	if(!atomic_dec(n->ref)) {
		mutex_free(n->lock);
		kh_destroy(pr, n->regs);
		kh_destroy(ln, n->links);
		free(n);
	}
}


// The caller is responsible for obtaining the node lock
static int tn_node_register(tn_node *n, patternreg *r) {
	// Get new ID
	do {
		if(++n->lastid <= 0)
			n->lastid = 1;
	} while(kh_get(pr, n->regs, n->lastid) != kh_end(n->regs));
	int id = n->lastid;

	// Insert in the table
	int ret;
	khiter_t k = kh_put(pr, n->regs, id, &ret);
	kh_val(n->regs, k) = r;

	// notify links
	k = kh_begin(n->links);
	for(; k != kh_end(n->links); ++k)
		if(kh_exist(n->links, k))
			tn_link_node_reg(kh_key(n->links, k), id, r);

	return id;
}


static inline void tn_node_dounreg(tn_node *n, patternreg *r, khiter_t k) {
	// notify links
	khiter_t i = kh_begin(n->links);
	for(; i != kh_end(n->links); ++i)
		if(kh_exist(n->links, i))
			tn_link_node_unreg(kh_key(n->links, i), kh_key(n->regs, k), r);

	// and remove registration
	kh_del(pr, n->regs, k);
	atomic_dec(r->active);
	tn_tuple_unref(r->pat);
	r->pat = NULL;
	patternreg_unref(r);
}

// Likewise, caller is responsible for obtaining the node lock
static void tn_node_unregister(tn_node *n, int id) {
	khiter_t k = kh_get(pr, n->regs, id);
	if(k != kh_end(n->regs)) {
		patternreg *r = kh_val(n->regs, k);
		tn_node_dounreg(n, r, k);
	}
}


static void tn_node_unregsession(tn_node *n, tn_session *s) {
	khiter_t k = kh_begin(n->regs);
	for(; k != kh_end(n->regs); ++k) {
		patternreg *r = kh_exist(n->regs, k) ? kh_val(n->regs, k) : NULL;
		if(r && r->type == 0 && r->recipient == s)
			tn_node_dounreg(n, r, k); // khash allows deletion from within a loop, yay!
	}
}


// The reference held on the tuple is passed to the node. So the following
// works without memory leaks:
//   tn_session_send(.., tn_tuple_new(..), ..);
static void tn_node_send(tn_node *n, tn_tuple *tup, tn_returnpath *rp, tn_link *src) {
	mutex_lock(n->lock);
	khiter_t k = kh_begin(n->regs);
	for(; k != kh_end(n->regs); ++k) {
		patternreg *r = kh_exist(n->regs, k) ? kh_val(n->regs, k) : NULL;
		if(!r || !tn_tuple_match(r->pat, tup))
			continue;
		if(r->type == 0) { // session
			if(rp && r->willreply)
				tn_returnpath_open(rp);
			tn_session_recv(r->recipient, tup, r, r->willreply ? rp : NULL);
		} else if(r->recipient != src) { // link (and not the sender of the tuple)
			if(rp)
				tn_returnpath_open(rp);
			tn_link_node_recv(r->recipient, tup, rp);
		}
	}
	mutex_unlock(n->lock);

	// Open-and-close the return path here, to ensure that the return path is
	// properly closed (e.g. session count decreases to zero) when there are no
	// interested sessions.
	if(rp) {
		tn_returnpath_open(rp);
		tn_reply_close(rp);
	}

	// Free the tuple if no sessions/links were interested in it.
	tn_tuple_unref(tup);
}






// Session

// A queued "message" for the session: either a regular tuple send() or a reply()
typedef struct sesmsg sesmsg;
struct sesmsg {
	sesmsg *next;
	tn_tuple *tup;
	patternreg *reg;    // If this is a regular send
	tn_returnpath *ret; // If this is a regular send that wishes to receive a reply.
	tn_returnpath *rep; // If this *is* a reply
};

struct tn_session {
	mutex_t(lock); // Protects q_begin and q_end
	tn_node *node;
	tn_session_dispatch_cb dispatch;
	sesmsg *q_begin;
	sesmsg *q_end;
	void *data;
	int active; // Whether this session is still alive (i.e. whether the application still uses it)
	int ref;    // Deferred free, because tn_returnpath references session objects as well
};


void tn_session_ref(tn_session *s) {
	atomic_inc(s->ref);
}


void tn_session_unref(tn_session *s) {
	if(!atomic_dec(s->ref)) {
		assert(!s->active);
		tn_node_unref(s->node);
		mutex_free(s->lock);
		free(s);
	}
}


tn_session *tn_session_new(tn_node *n, tn_session_dispatch_cb dis, void *data) {
	assert(dis);
	assert(n);
	tn_session *s = malloc(sizeof(tn_session));
	mutex_init(s->lock);
	s->node = n;
	tn_node_ref(n);
	s->ref = 1;
	s->active = 1;
	s->q_begin = s->q_end = NULL;
	s->dispatch = dis;
	s->data = data;
	return s;
}


void tn_session_send(tn_session *s, tn_tuple *t, tn_reply_cb cb, void *dat) {
	assert(s);
	assert(t);
	tn_node_send(s->node, t, cb ? tn_returnpath_new_ses(s, cb, dat) : NULL, NULL);
}


int tn_session_register(tn_session *s, tn_tuple *pat, int willreply, tn_tuple_cb cb, void *dat) {
	assert(s);
	assert(pat);
	assert(cb);
	mutex_lock(s->node->lock);
	int id = tn_node_register(s->node, patternreg_new_ses(pat, willreply, s, cb, dat));
	mutex_unlock(s->node->lock);
	return id;
}


void tn_session_unregister(tn_session *s, int id) {
	assert(s);
	mutex_lock(s->node->lock);
	tn_node_unregister(s->node, id);
	mutex_unlock(s->node->lock);
}


// Assumes s->lock is held, and will unlock it.
static inline void tn_session_appendmsg(tn_session *s, sesmsg *m) {
	m->next = NULL;
	int empty = !s->q_begin;
	if(empty)
		s->q_begin = m;
	else
		s->q_end->next = m;
	s->q_end = m;
	mutex_unlock(s->lock);
	if(empty)
		s->dispatch(s, s->data);
}

// Called from the node, when we've received a tuple. Note that *ret has
// already been open()'ed for this session.
static void tn_session_recv(tn_session *s, tn_tuple *tup, patternreg *pr, tn_returnpath *ret) {
	tn_tuple_ref(tup);
	patternreg_ref(pr);
	sesmsg *m = malloc(sizeof(sesmsg));
	m->tup = tup;
	m->reg = pr;
	m->ret = ret;
	m->rep = NULL;
	mutex_lock(s->lock);
	tn_session_appendmsg(s, m);
}


// Called when we've received a reply on one of our return-paths.
static void tn_session_reply(tn_session *s, tn_tuple *tup, tn_returnpath *rep) {
	// Session has been close()'ed, ignore this message
	mutex_lock(s->lock);
	if(!s->active) {
		mutex_unlock(s->lock);
		tn_tuple_unref(tup);
		if(!tup)
			tn_returnpath_free(rep);
		return;
	}
	// Otherwise, queue it
	sesmsg *m = malloc(sizeof(sesmsg));
	m->tup = tup;
	m->reg = NULL;
	m->ret = NULL;
	m->rep = rep;
	tn_session_appendmsg(s, m);
}


static inline void tn_session_dispatch_one(tn_session *s, sesmsg *m) {
	// Regular send()
	if(m->reg) {
		if(m->reg->active) // If the pattern hasn't been unregistered in the meantime
			m->reg->cb(s, m->tup, m->ret, m->reg->data); // Pass our reference on m->tup to the application
		else {
			tn_tuple_unref(m->tup);
			if(m->ret)
				tn_reply_close(m->ret);
		}
		patternreg_unref(m->reg);

	// Reply
	} else {
		m->rep->t.s.cb(s, m->tup, m->rep->t.s.data); // Again, pass our reference on m->tup
		if(!m->tup)
			tn_returnpath_free(m->rep);
	}
	free(m);
}


// Returns 1 if the session is still alive, 0 if it has been closed. It is
// assumed that the caller of this function has a ref() on the session.
int tn_session_dispatch(tn_session *s) {
	mutex_lock(s->lock);

	while(s->q_begin) {
		assert(s->active); // Queue must be (and stay) empty if the session has been closed.
		// Pop item from the queue
		sesmsg *q = s->q_begin;
		s->q_begin = q->next;
		// And run the callback (outside of the lock)
		mutex_unlock(s->lock);
		tn_session_dispatch_one(s, q);
		mutex_lock(s->lock);
	};

	int a = s->active;
	mutex_unlock(s->lock);
	return a;
}


void tn_session_close(tn_session *s) {
	assert(s);

	mutex_lock(s->node->lock);
	tn_node_unregsession(s->node, s);
	mutex_unlock(s->node->lock);

	// free the message queue and close any return-paths
	mutex_lock(s->lock);
	atomic_dec(s->active);
	sesmsg *n, *c;
	for(n=s->q_begin; n; n=c) {
		c = n->next;
		if(n->ret)
			tn_reply_close(n->ret);
		if(n->rep && !n->tup) // close, free the return-path
			tn_returnpath_free(n->rep);
		if(n->reg)
			patternreg_unref(n->reg);
		if(n->tup)
			tn_tuple_unref(n->tup);
		free(n);
	}
	s->q_begin = s->q_end = NULL;
	mutex_unlock(s->lock);

	s->dispatch(s, s->data);
	tn_session_unref(s);
}






// Link

struct tn_link {
	mutex_t(lock);
	tn_node *node;
	tn_link_context *ctx;
	tn_link_error_cb errcb;
	tn_link_ready_cb readycb;
	int errcode;
	char *errmsg;
	void *data;
	lbuf rbuf; // not protected by the lock
	lbuf wbuf;
	lbuf tbuf; // (temporary) buffer that we gave to the context
	unsigned handshaked : 1;
	unsigned sync : 1;
	unsigned synced : 1;
	unsigned readydone : 1;
	unsigned active : 1;
	int catchall; // registration ID of our catch-all tuple (0 if not registered)
	int lastid; // last used ID for an outgoing tuple with a returnpath
	khash_t(ii) *regs; // map of remote pattern IDs to local IDs
	khash_t(rp) *rets; // map of remote returnpath IDs to local returnpaths
	void *lasttup; // last received tuple (void because it's only used for pointer comparison)
	int ref;
};


void tn_link_ref(tn_link *l) {
	atomic_inc(l->ref);
}


void tn_link_unref(tn_link *l) {
	if(!atomic_dec(l->ref)) {
		assert(!l->active);

		// And free
		lbuf_free(l->rbuf);
		if(l->errmsg)
			free(l->errmsg);
		tn_node_unref(l->node);
		mutex_free(l->lock);
		free(l);
	}
}


// TODO: configure a maximum buffer size?
tn_link *tn_link_new(tn_node *n, tn_link_context *ctx, void *data) {
	tn_link *l = malloc(sizeof(tn_link));
	l->node = n;
	l->ctx = ctx;
	l->data = data;
	l->handshaked = 0;
	l->errmsg = NULL;
	l->errcode = 0;
	l->errcb = NULL;
	l->readycb = NULL;
	l->ref = 1;
	l->active = 1;
	l->sync = 1;
	l->synced = l->readydone = 0;
	l->lasttup = NULL;
	tn_node_ref(n);
	mutex_init(l->lock);
	lbuf_init(l->rbuf);
	lbuf_init(l->wbuf);
	lbuf_init(l->tbuf);
	l->lastid = 0;
	l->catchall = 0;
	l->regs = kh_init(ii);
	l->rets = kh_init(rp);
	return l;
}


void tn_link_set_error(tn_link *l, int code, const char *msg) {
	// Set active to 0 inside a lock. After unlocking, this guarantees nobody
	// else will touch the state data protected by the active flag.
	mutex_lock(l->lock);
	int a = l->active;
	l->active = 0;
	mutex_unlock(l->lock);
	if(!a)
		return;

	mutex_lock(l->node->lock);

	// Unregister any patterns
	khiter_t k = kh_begin(l->regs);
	for(; k!=kh_end(l->regs); k++)
		if(kh_exist(l->regs, k))
			tn_node_unregister(l->node, kh_val(l->regs, k));

	if(l->catchall)
		tn_node_unregister(l->node, l->catchall);

	// Remove link from the notification list
	k = kh_get(ln, l->node->links, l);
	if(k != kh_end(l->node->links))
		kh_del(ln, l->node->links, k);

	mutex_unlock(l->node->lock);

	// Close any local return paths
	for(k=kh_begin(l->rets); k!=kh_end(l->regs); k++)
		if(kh_exist(l->rets, k))
			tn_reply_close(kh_val(l->rets, k));

	kh_destroy(ii, l->regs);
	kh_destroy(rp, l->rets);
	l->regs = NULL;
	l->catchall = 0;
	lbuf_free(l->wbuf);
	lbuf_init(l->wbuf);
	lbuf_free(l->tbuf);
	lbuf_init(l->tbuf);
	l->errcode = code;
	l->errmsg = msg ? strdup(msg) : NULL;
	l->ctx->dispatch(l, l->data);
}


// Assumes that the lock is held. Should be called to indicate that something
// has been written to the write buffer.
static inline void tn_link_write(tn_link *l) {
	// If tbuf isn't empty, we're done here.
	if(l->tbuf.len)
		return;

	// Now try a write.
	// TODO: The write() function currently has no way of (directly) reporting
	// back an error, because tn_link_set_error locks. Is there even a context
	// that would be able to do an immediate write? Might as well disallow
	// write() to do anything except dispatching a start/endwrite.
	int n = l->ctx->write(l, l->wbuf.dat, l->wbuf.len, l->data);

	// Something has been written
	if(l->wbuf.len && n > 0)
		lbuf_shift(l->wbuf, n);
}


// Returns a buffer that should be written to the network. Returns 0 if there's
// nothing to write, in which case *buf is not modified. Call
// tn_link_endwrite() when done.
int tn_link_startwrite(tn_link *l, char **buf) {
	mutex_lock(l->lock);

	// If we have nothing to write or if there's already a write busy in the
	// background, just return 0.
	if(!l->active || !l->wbuf.len || l->tbuf.len) {
		mutex_unlock(l->lock);
		return 0;
	}

	// Otherwise, move wbuf to tbuf and re-init wbuf so that we still have a
	// place to write data to while the context is busy writing something to
	// the network.
	l->tbuf = l->wbuf;
	lbuf_init(l->wbuf);

	// We (and the context) are now the only ones in control of tbuf.
	mutex_unlock(l->lock);
	*buf = l->tbuf.dat;
	return l->tbuf.len;
}


// End a write, indicating how many bytes have been written to the network.
// It's best *NOT* to call this funcion when tn_link_startwrite() returned 0.
// This function must always be called, even if _close() or _set_error() has
// been called in the mean time. (Otherwise the temporary buffer won't be
// freed)
void tn_link_endwrite(tn_link *l, int len) {
	mutex_lock(l->lock);

	// Error/close, just free the temp buffer and return
	if(!l->tbuf.len || !l->active) {
		lbuf_free(l->tbuf);
		lbuf_init(l->tbuf);
		mutex_unlock(l->lock);
		return;
	}

	if(len > 0)
		lbuf_shift(l->tbuf, len);


	// If the temp. buffer has been emptied, just free it and leave the write buffer alone.
	if(!l->tbuf.len)
		lbuf_free(l->tbuf);
	else {
		// Oops, someone else has written to the write buffer while the writer
		// was being busy. Append this new data to the temp buffer.
		if(l->wbuf.len)
			lbuf_append(l->tbuf, l->wbuf.dat, l->wbuf.len);
		// Now give back our temporary buffer
		if(l->wbuf.dat)
			free(l->wbuf.dat);
		l->wbuf = l->tbuf;
	}
	lbuf_init(l->tbuf);
	mutex_unlock(l->lock);
}


void tn_link_on_error(tn_link *l, tn_link_error_cb cb) {
	l->errcb = cb;
}


void tn_link_on_ready(tn_link *l, tn_link_ready_cb cb) {
	l->readycb = cb;
}


void tn_link_set_sync(tn_link *l, int sync) {
	assert(!l->handshaked && "Don't call tn_link_set_sync() after tn_link_start()!");
	l->sync = !!sync;
}


void tn_link_start(tn_link *l) {
	mutex_lock(l->lock);
	assert(l->active && !l->handshaked);
	lbuf_append(l->wbuf, "ver,1.0 seri,json sero,json\n", 28);
	tn_link_write(l);
	mutex_unlock(l->lock);
}


static inline void tn_link_write_reg(tn_link *l, int id, tn_tuple *pat) {
	char prefix[30];
	int n = snprintf(prefix, 30, "[2,%d,", id);
	lbuf_append(l->wbuf, prefix, n);
	tn_json_fmt_buf(pat, &l->wbuf);
	lbuf_append(l->wbuf, "]\n", 2);
}


// Someone registered a pattern with the node.
static void tn_link_node_reg(tn_link *l, int id, patternreg *p) {
	// Don't lock the mutex before this check, otherwise we'll deadlock (since
	// the registration came from this link).
	if(p->recipient == l)
		return;
	mutex_lock(l->lock);
	if(l->active) {
		tn_link_write_reg(l, id, p->pat);
		tn_link_write(l);
	}
	mutex_unlock(l->lock);
}


// Someone unregistered a pattern with the node.
static void tn_link_node_unreg(tn_link *l, int id, patternreg *p) {
	if(p->recipient == l)
		return;
	mutex_lock(l->lock);
	if(l->active) {
		char msg[30];
		int n = snprintf(msg, 30, "[4,%d]\n", id);
		lbuf_append(l->wbuf, msg, n);
		tn_link_write(l);
	}
	mutex_unlock(l->lock);
}


// We've received a tuple from the node (couldn't have been ours, the node
// filters those out automatically).
static void tn_link_node_recv(tn_link *l, tn_tuple *tup, tn_returnpath *r) {
	mutex_lock(l->lock);
	if(l->active && tup != l->lasttup) {
		l->lasttup = tup;

		// Handle return path
		int id = 0;
		if(r) {
			do {
				if(++l->lastid <= 0)
					l->lastid = 1;
			} while(kh_get(rp, l->rets, l->lastid) != kh_end(l->rets));
			id = l->lastid;

			int ret;
			khiter_t k = kh_put(rp, l->rets, id, &ret);
			kh_val(l->rets, k) = r;
		}

		// Send message
		char prefix[30];
		int n = snprintf(prefix, 30, "[5,%d,", id);
		lbuf_append(l->wbuf, prefix, n);
		tn_json_fmt_buf(tup, &l->wbuf);
		lbuf_append(l->wbuf, "]\n", 2);
		tn_link_write(l);
	} else
		tn_reply_close(r);
	mutex_unlock(l->lock);
}


static void tn_link_reply(tn_link *l, tn_tuple *tup, tn_returnpath *r) {
	mutex_lock(l->lock);
	if(l->active && !tup) { // close
		char msg[30];
		int n = snprintf(msg, 30, "[7,%d]\n", r->t.l.id);
		lbuf_append(l->wbuf, msg, n);
		tn_link_write(l);
	} else if(l->active && tup) { // reply
		char prefix[30];
		int n = snprintf(prefix, 30, "[6,%d,", r->t.l.id);
		lbuf_append(l->wbuf, prefix, n);
		tn_json_fmt_buf(tup, &l->wbuf);
		lbuf_append(l->wbuf, "]\n", 2);
		tn_link_write(l);
	}
	mutex_unlock(l->lock);
	if(!tup)
		tn_returnpath_free(r);
	else
		tn_tuple_unref(tup);
}


#define inc(n) do { buf+=n; len-=n; } while(0)

// Called while the lock is held.
static inline void tn_link_handlehandshake(tn_link *l, const char *buf, int len) {
	int ok = 0;
	while(len > 0) {
		// Space, ignore
		if(*buf == ' ') {
			inc(1);
			continue;
		}

		// Version
		if(len > 4 && strncmp(buf, "ver,", 4) == 0) {
			inc(3);
			while(len && *buf != ' ') {
				if(len >= 3 && strncmp(buf, ",1.", 3) == 0)
					ok |= 1;
				inc(1);
			}
			continue;
		}

		// Ser[io]
		if(len > 5 && (strncmp(buf, "seri,", 5) == 0 || strncmp(buf, "sero,", 5) == 0)) {
			int flag = buf[3] == 'i' ? 2 : 4;
			inc(4);
			while(len && *buf != ' ') {
				if(len >= 5 && strncmp(buf, ",json", 5) == 0 && (len == 5 || buf[5] == ' ' || buf[5] == ','))
					ok |= flag;
				inc(1);
			}
			continue;
		}

		// Unknown parameter
		while(len > 0 && *buf != ' ')
			inc(1);
	}

	if(ok != (1|2|4)) {
		int c = !(ok&1) ? 1 : !(ok&2)? 2 : 3;
		tn_link_set_error(l, -c,
			c==1 ? "No or invalid protocol version" : c==2 ? "No common input format" : "No common output format");
	} else
		l->handshaked = 1;

	// If we want sync with their pattern list, request this. Otherwise, register the catch-all pattern with the node.
	if(l->sync) {
		mutex_lock(l->lock);
		if(l->active) {
			lbuf_append(l->wbuf, "[1,true]\n", 9); // patternsync
			tn_link_write(l);
		}
		mutex_unlock(l->lock);
	} else {
		mutex_lock(l->node->lock);
		mutex_lock(l->lock);
		if(l->active)
			l->catchall = tn_node_register(l->node, patternreg_new_link(tn_tuple_new(""), l));
		mutex_unlock(l->lock);
		mutex_unlock(l->node->lock);
		l->synced = 1;
		l->ctx->dispatch(l, l->data);
	}
}


#define incs() do { while(len > 0 && (*buf == 0x20 || *buf == 0x09 || *buf == 0x0A || *buf == 0x0D)) { inc(1); } } while(0)

static int tn_link_parsemessage(tn_link *l, const char *buf, int len, int32_t *arg, tn_tuple **tup) {
	if(len < 2 || *buf != '[') {
		tn_link_set_error(l, -4, "Invalid message format.");
		return 0;
	}
	inc(1);
	incs();
	if(!len) {
		tn_link_set_error(l, -4, "Invalid message format.");
		return 0;
	}
	// Message type
	if(*buf < '1' || *buf > '7') {
		tn_link_set_error(l, -5, "Invalid message type.");
		return 0;
	}
	int type = *buf-'0';
	inc(1);
	incs();
	// Boolean argument
	if(type == 1) {
		// Comma
		if(*buf != ',') {
			tn_link_set_error(l, -4, "Invalid message format.");
			return 0;
		}
		inc(1);
		// Bool
		if(len >= 4 && strncmp(buf, "true", 4) == 0) {
			*arg = 1;
			inc(4);
		} else if(len >= 5 && strncmp(buf, "false", 5) == 0) {
			*arg = 0;
			inc(5);
		} else {
			tn_link_set_error(l, -4, "Invalid message format.");
			return 0;
		}
		incs();
	}
	// Integer argument
	if(type == 2 || type == 4 || type == 5 || type == 6 || type == 7) {
		// Comma
		if(*buf != ',') {
			tn_link_set_error(l, -4, "Invalid message format.");
			return 0;
		}
		inc(1);
		incs();
		// Integer (this parser only handles positive integers and isn't as strict as the above JSON parser)
		*arg = 0;
		while(len > 0 && *buf >= '0' && *buf <= '9') {
			int n = (*arg)*10 + (*buf-'0');
			if(n < *arg) {
				tn_link_set_error(l, -4, "Invalid message format.");
				return 0;
			}
			*arg = n;
			inc(1);
		}
		incs();
	}
	// Tuple argument
	if(type == 2 || type == 5 || type == 6) {
		// Comma
		if(*buf != ',') {
			tn_link_set_error(l, -4, "Invalid message format.");
			return 0;
		}
		inc(1);
		incs();
		// Tuple
		int rd = 0;
		*tup = tn_json_parse(buf, len, &rd);
		if(!*tup) {
			tn_link_set_error(l, -6, "Invalid JSON tuple.");
			return 0;
		}
		buf += rd;
		len -= rd;
		incs();
	}
	// End-of-message
	if(!len || *buf != ']') {
		tn_link_set_error(l, -4, "Invalid message format.");
		return 0;
	}
	inc(1);
	incs();
	if(len) {
		tn_link_set_error(l, -7, "Excessive data after message.");
		return 0;
	}
	return type;
}

#undef inc
#undef incs


static void tn_link_handlemessage(tn_link *l, int type, int32_t arg, tn_tuple *tup) {
	switch(type) {

	case 1: // patternsync 1/0
		mutex_lock(l->node->lock);
		mutex_lock(l->lock);
		khiter_t k = kh_get(ln, l->node->links, l);
		if(l->active && arg) {
			int ret;
			// Add this link to the node for notifications
			if(k == kh_end(l->node->links))
				kh_put(ln, l->node->links, l, &ret);
			// Send the current pattern list
			for(k = kh_begin(l->node->regs); k!=kh_end(l->node->regs); ++k)
				if(kh_exist(l->node->regs, k))
					tn_link_write_reg(l, kh_key(l->node->regs, k), kh_val(l->node->regs, k)->pat);
			// Send a regdone message
			lbuf_append(l->wbuf, "[3]\n", 4);
			tn_link_write(l);
		} else if(l->active) {
			// Remove this link from the notification list
			if(k != kh_end(l->node->links))
				kh_del(ln, l->node->links, k);
		}
		mutex_unlock(l->lock);
		mutex_unlock(l->node->lock);
		break;

	case 2: // register pid pat
		if(!l->sync) {
			tn_tuple_unref(tup);
			break;
		}
		mutex_lock(l->node->lock);
		mutex_lock(l->lock);
		if(l->active) {
			int id = tn_node_register(l->node, patternreg_new_link(tup, l));
			khiter_t k = kh_get(ii, l->regs, arg);
			if(k != kh_end(l->regs))
				tn_node_unregister(l->node, kh_val(l->regs, k));
			else {
				int ret;
				k = kh_put(ii, l->regs, id, &ret);
			}
			kh_val(l->regs, k) = id;
		}
		mutex_unlock(l->lock);
		mutex_unlock(l->node->lock);
		break;

	case 3: // regdone
		l->synced = 1;
		l->ctx->dispatch(l, l->data);
		break;

	case 4: // unregister pid
		if(!l->sync)
			break;
		mutex_lock(l->node->lock);
		mutex_lock(l->lock);
		if(l->active) {
			khiter_t k = kh_get(ii, l->regs, arg);
			if(k != kh_end(l->regs)) {
				tn_node_unregister(l->node, kh_val(l->regs, k));
				kh_del(ii, l->regs, k);
			}
		}
		mutex_unlock(l->lock);
		mutex_unlock(l->node->lock);
		break;

	case 5: // tuple tid tuple
		tn_node_send(l->node, tup, arg > 0 ? tn_returnpath_new_link(l, arg) : NULL, l);
		break;

	case 6: { // reply tid tuple
		tn_returnpath *r = NULL;
		mutex_lock(l->lock);
		if(l->active) {
			khiter_t k = kh_get(rp, l->rets, arg);
			if(k != kh_end(l->rets))
				r = kh_val(l->rets, k);
		}
		tn_reply(r, tup);
		mutex_unlock(l->lock);
		break;
	}

	case 7: { // close tid
		tn_returnpath *r = NULL;
		mutex_lock(l->lock);
		if(l->active) {
			khiter_t k = kh_get(rp, l->rets, arg);
			if(k != kh_end(l->rets)) {
				r = kh_val(l->rets, k);
				kh_del(rp, l->rets, k);
			}
		}
		tn_reply_close(r);
		mutex_unlock(l->lock);
		break;
	}

	default:
		assert(0);
	}
}


// Tries to read a single message from the buffer, returns the number of bytes
// read (0 if there is no complete message).
static int tn_link_handleread(tn_link *l, const char *buf, int len) {
	// All messages end with a newline (in the current protocol), so that's
	// easy to detect.
	char *end;
	if((end = memchr(buf, '\n', len)) == NULL)
		return 0;
	len = end-buf;
	// Throw away '\r' if there is one
	int msglen = len > 0 && buf[len-1] == '\r' ? len-1 : len;
	if(!l->handshaked)
		tn_link_handlehandshake(l, buf, msglen);
	else {
		int32_t arg = 0;
		tn_tuple *tup = NULL;
		int type = tn_link_parsemessage(l, buf, msglen, &arg, &tup);
		if(type)
			tn_link_handlemessage(l, type, arg, tup);
	}

	return len+1;
}


// Calls to this function must be serialized.
void tn_link_read(tn_link *l, const char *buf, int len) {
	if(!l->active)
		return;

	// Don't copy things to the read buffer if we have a full message
	int n=1;
	while(!l->rbuf.len && len > 0 && n) {
		n = tn_link_handleread(l, buf, len);
		if(!l->active)
			return;
		buf += n;
		len -= n;
	}

	// Copy leftover data to the read buffer
	if(len)
		lbuf_append(l->rbuf, buf, len);

	// Handle / remove stuff from the read buffer
	// TODO: tn_link_handleread() does a linear search on the buffer to find
	// '\n'. It's not very efficient if it has to do this all over again for
	// every increase in the buffer, since only the newly appended bytes
	// actually have to be checked.
	int r=0;
	n=1;
	while(l->rbuf.len-r > 0 && n && l->active) {
		n = tn_link_handleread(l, l->rbuf.dat+r, l->rbuf.len-r);
		if(!l->active)
			return;
		r += n;
	}
	if(r > 0)
		lbuf_shift(l->rbuf, r);
}


// Same notes as tn_session_dispatch() with regard to return value and reference.
int tn_link_dispatch(tn_link *l) {
	mutex_lock(l->lock);

	// If there's an error, handle that.
	if(l->errcode) {
		mutex_unlock(l->lock);
		if(l->errcb)
			l->errcb(l, l->errcode, l->errmsg);
		tn_link_close(l);
		return 0;
	}

	int a = l->active;
	mutex_unlock(l->lock);

	// If we've synced, run the ready callback
	if(l->active && l->readycb && l->synced && !l->readydone) {
		l->readydone = 1;
		l->readycb(l);
	}
	return a;
}


// TODO: This is a force-close. Perhaps a more graceful "flush buffers and
// shutdown" close should also be implemented.
void tn_link_close(tn_link *l) {
	tn_link_set_error(l, 0, NULL);
	tn_link_unref(l);
}

// vim:noet:sw=4:ts=4
