/*
  Copyright (c) 2012 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <assert.h>
#include <unistd.h>
#include "tanja.h"
#include "tanja_pthread.h"

static tn_link *lnk;
static tn_session *ses;


// When the tuple ["quit",..] is received, the link and the session will be
// closed. Hopefully resulting in a graceful shutdown.
static void tup_quit(tn_session *s, tn_tuple *tup, tn_returnpath *p, void *d) {
	assert(s == ses);
	assert(p == NULL);
	assert(d == NULL);
	fprintf(stderr, "Quit tuple received, closing\n");
	if(lnk)
		tn_link_close(lnk);
	tn_session_close(s);
	tn_tuple_unref(tup);
}


// Print out any received tuples for debugging. Whether this is called before
// tup_quit() is not defined.
static void tup_all(tn_session *s, tn_tuple *tup, tn_returnpath *p, void *d) {
	assert(s == ses);
	assert(p == NULL);
	assert(d == NULL);
	char *t = tn_json_fmt(tup);
	fprintf(stderr, "Tuple: %s\n", t);
	free(t);
	tn_tuple_unref(tup);
}


// Upon receiving this, the session will reply to the returnpath with "was?"
// and then close it.
static void tup_hallo(tn_session *s, tn_tuple *tup, tn_returnpath *p, void *d) {
	assert(s == ses);
	assert(d == NULL);
	tn_reply(p, tn_tuple_new("s", strdup("was?")));
	tn_reply_close(p);
	tn_tuple_unref(tup);
}


static void tup_request_reply(tn_session *s, tn_tuple *tup, void *d) {
	assert(s == ses);
	if(tup) {
		char *t = tn_json_fmt(tup);
		fprintf(stderr, "Reply to path #%d: %s\n", (int)(long)d, t);
		free(t);
		tn_tuple_unref(tup);
	} else
		fprintf(stderr, "Path #%d closed.\n", (int)(long)d);
}


// Upon receicing this, the session will send out a "please" tuple, for which
// it expects to receive a reply.
static void tup_request(tn_session *s, tn_tuple *tup, tn_returnpath *p, void *d) {
	static long num = 0;
	assert(s == ses);
	assert(p == NULL);
	assert(d == NULL);
	tn_session_send(ses, tn_tuple_new("s", strdup("please")), tup_request_reply, (void *)++num);
	tn_tuple_unref(tup);
}


static void lnk_error(tn_link *l, int code, char *msg) {
	assert(l == lnk);
	fprintf(stderr, "Link error #%d: %s\n", code, msg?msg:"(empty)");
	// Indicate that the other session shouldn't _close() the link anymore.
	lnk = NULL;
	// Signal to the other session that it can close.
	// (tn_session_send() may be called from any context)
	tn_session_send(ses, tn_tuple_new("s", strdup("quit")), NULL, NULL);
}


static void lnk_ready(tn_link *l) {
	assert(l == lnk);
	fprintf(stderr, "Link ready for sending!\n");
	tn_session_send(ses, tn_tuple_new("ss", strdup("hello"), strdup("world")), NULL, NULL);
}


int main() {
	tn_pthread_setsig();
	tn_node *n = tn_node_new();

	// Create a session for debugging
	pthread_t tha;
	ses = tn_session_pthread(n, &tha);
	tn_session_register(ses, tn_tuple_new(""), 0, tup_all, NULL);
	tn_session_register(ses, tn_tuple_new("s", strdup("quit")), 0, tup_quit, NULL);
	tn_session_register(ses, tn_tuple_new("s", strdup("hallo")), 1, tup_hallo, NULL);
	tn_session_register(ses, tn_tuple_new("s", strdup("request")), 0, tup_request, NULL);

	// Create a link with STDIO
	pthread_t thb;
	lnk = tn_link_pthread_fd(n, STDIN_FILENO, STDOUT_FILENO, &thb);
	tn_link_on_ready(lnk, lnk_ready);
	tn_link_on_error(lnk, lnk_error);
	tn_link_start(lnk);

	// Close
	tn_node_unref(n);
	pthread_join(tha, NULL);
	pthread_join(thb, NULL);
	return 0;
}

// vim:noet:sw=4:ts=4
