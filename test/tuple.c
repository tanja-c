/*
  Copyright (c) 2012 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tanja.h>


static int test_num = 0;

#define t_assert(n) printf("%sok %d - " __FILE__ ":%d: " #n "\n", (n)?"":"not ", ++test_num, __LINE__)

int main() {
	tn_el e, v;
	int64_t i;
	double d;
	char *s;
	char buf[25];
	int k;

	e = tn_el_new(TN_VT_INT, (int64_t)1023);
	t_assert(e.type == TN_VT_INT);
	t_assert(e.v.i == (int64_t)1023);
	t_assert(tn_el_intval(e, NULL));
	t_assert(tn_el_intval(e, &i));
	t_assert(i == 1023);
	t_assert(tn_el_int(e) == 1023);
	t_assert(tn_el_numval(e, NULL));
	t_assert(tn_el_numval(e, &d));
	t_assert(d == 1023.0);
	t_assert(tn_el_num(e) == 1023.0);
	s = tn_el_str(e, buf);
	t_assert(s != NULL && strcmp(s, "1023") == 0);
	tn_el_free(e);

	e = tn_el_new('n', 102.3);
	t_assert(e.type == TN_VT_NUM);
	t_assert(e.v.n == 102.3);
	t_assert(tn_el_intval(e, NULL));
	t_assert(tn_el_intval(e, &i));
	t_assert(i == 102);
	t_assert(tn_el_numval(e, NULL));
	t_assert(tn_el_numval(e, &d));
	t_assert(d == 102.3);
	s = tn_el_str(e, buf);
	t_assert(s != NULL && strcmp(s, "102.3") == 0);
	tn_el_free(e);

	e = tn_el_new('n', 10e50);
	t_assert(e.type == TN_VT_NUM);
	t_assert(e.v.n == 10e50);
	t_assert(!tn_el_intval(e, &i));
	t_assert(!tn_el_intval(e, NULL));
	t_assert(tn_el_int(e) == 0);
	tn_el_free(e);

	e = tn_el_new('s', strdup("1021"));
	t_assert(e.type == TN_VT_STR);
	t_assert(strcmp(e.v.s, "1021") == 0);
	t_assert(tn_el_intval(e, NULL));
	t_assert(tn_el_intval(e, &i));
	t_assert(i == 1021);
	t_assert(tn_el_int(e) == 1021);
	t_assert(tn_el_numval(e, NULL));
	t_assert(tn_el_numval(e, &d));
	t_assert(d == 1021.0);
	t_assert(tn_el_num(e) == 1021.0);
	s = tn_el_str(e, buf);
	t_assert(s != NULL && strcmp(s, "1021") == 0);
	s = tn_el_str(e, NULL);
	t_assert(s != NULL && strcmp(s, "1021") == 0);
	tn_el_free(e);

	e = tn_el_new('s', strdup("1021:"));
	t_assert(e.type == TN_VT_STR);
	t_assert(strcmp(e.v.s, "1021:") == 0);
	t_assert(!tn_el_intval(e, NULL));
	t_assert(!tn_el_numval(e, NULL));
	tn_el_free(e);

	e = tn_el_new('s', strdup("1021.2"));
	t_assert(tn_el_int(e) == 1021);
	t_assert(tn_el_num(e) == 1021.2);
	tn_el_free(e);

	e = tn_map_new(0, "si", strdup("str"), strdup("strval"), strdup("int"), (uint64_t)42);
	t_assert(e.type == TN_VT_MAP);
	t_assert(e.count == 2);
	v = tn_map_get(e, "str");
	t_assert(tn_el_isvalid(v));
	t_assert(v.type == TN_VT_STR);
	t_assert(v.v.s && strcmp(v.v.s, "strval") == 0);
	v = tn_map_get(e, "b");
	t_assert(!tn_el_isvalid(v));
	v = tn_map_get(e, "int");
	t_assert(tn_el_isvalid(v));
	t_assert(v.type == TN_VT_INT);
	t_assert(v.v.i == 42);
	tn_el_free(e);

	e = tn_map_new(0, "");
	t_assert(e.count == 0);
	for(k=0; k<10000; k++) {
		snprintf(buf, 25, "%d", k);
		tn_map_set(&e, "i", strdup(buf), (int64_t)k);
	}
	t_assert(e.count == k);
	v = tn_map_get(e, "1234");
	t_assert(tn_el_int(v) == 1234);
	v = tn_map_get(e, "0");
	t_assert(tn_el_int(v) == 0);
	v = tn_map_get(e, "9999");
	t_assert(tn_el_int(v) == 9999);
	for(k=5000; k<15000; k++) {
		snprintf(buf, 25, "%d", k);
		tn_map_set(&e, "i", strdup(buf), (int64_t)(k*100));
	}
	t_assert(e.count == k);
	v = tn_map_get(e, "1234");
	t_assert(tn_el_int(v) == 1234);
	v = tn_map_get(e, "9999");
	t_assert(tn_el_int(v) == 999900);
	v = tn_map_get(e, "14999");
	t_assert(tn_el_int(v) == 1499900);
	tn_el_free(e);

	printf("1..%d\n", test_num);
	return 0;
}

// vim:noet:sw=4:ts=4
