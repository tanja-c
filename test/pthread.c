/*
  Copyright (c) 2012 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include "tanja.h"
#include "tanja_pthread.h"

#define t_assert(n) printf("%sok %d - " __FILE__ ":%d: " #n "\n", (n)?"":"not ", ++test_num, __LINE__)

static int test_num = 0;
static int r_quit = 0;
static int r_alpha = 0;
static int r_alpha_sb = 0;
static int r_reply = 0;
static int reg_alpha;
static tn_session *sa, *sb;


static void recv_alpha(tn_session *s, tn_tuple *tup, tn_returnpath *p, void *d) {
	printf("# alpha %d\n", r_alpha);
	t_assert(sa == s);
	t_assert(d == (void *)1);
	t_assert(p == NULL);
	t_assert(tup && tup->n == 2);
	t_assert(tup->e[0].type == TN_VT_STR && tup->e[0].v.s != NULL && strcmp(tup->e[0].v.s, "alpha") == 0);
	t_assert(tup->e[1].type == TN_VT_INT && tup->e[1].v.i == r_alpha);
	t_assert(r_quit < 2);
	tn_tuple_unref(tup);
	if(++r_alpha >= 5) {
		tn_session_unregister(s, reg_alpha);
		// Send quit when we're done
		tn_session_send(sa, tn_tuple_new("s", strdup("quit")), NULL, NULL);
	}
}


// Note: This function may run in parallel with recv_alpha 0, so the test
// output may interleave.
static void recv_alpha_sb(tn_session *s, tn_tuple *tup, tn_returnpath *p, void *d) {
	printf("# alpha_sb\n");
	t_assert(sb == s);
	t_assert(d == (void *)2);
	t_assert(p != NULL);
	t_assert(tup && tup->n == 2);
	t_assert(tup->e[0].type == TN_VT_STR && tup->e[0].v.s != NULL && strcmp(tup->e[0].v.s, "alpha") == 0);
	t_assert(tup->e[1].type == TN_VT_INT && tup->e[1].v.i == 0);
	t_assert(r_quit == 0);
	tn_tuple_unref(tup);

	// Reply
	tn_reply(p, tn_tuple_new("i", (uint64_t)1));
	tn_reply(p, tn_tuple_new("i", (uint64_t)2));
	tn_reply_close(p);

	// Send some more alphas
	int i;
	for(i=1; i<10; i++)
		tn_session_send(sa, tn_tuple_new("si", strdup("alpha"), (uint64_t)i), NULL, NULL);

	tn_session_close(sb);
	r_alpha_sb++;
}


static void recv_quit(tn_session *s, tn_tuple *tup, tn_returnpath *p, void *d) {
	printf("# quit %d\n", r_quit);
	t_assert(sa == s);
	t_assert(p == NULL);
	t_assert(d == NULL);
	t_assert(r_alpha >= 1);
	t_assert(tup && tup->n == 1);
	t_assert(tup->e[0].type == TN_VT_STR && tup->e[0].v.s != NULL && strcmp(tup->e[0].v.s, "quit") == 0);
	tn_tuple_unref(tup);
	if(r_quit++)
		tn_session_close(s);
}


static void recv_reply(tn_session *s, tn_tuple *tup, void *d) {
	printf("# reply %d\n", r_reply);
	t_assert(sa == s);
	t_assert(d == (void *)3);
	if(++r_reply <= 2) {
		t_assert(tup && tup->n == 1);
		t_assert(tup->e[0].type == TN_VT_INT && tup->e[0].v.i == r_reply);
		tn_tuple_unref(tup);
	} else {
		t_assert(tup == NULL);
		// Send quit when we're done
		tn_session_send(sa, tn_tuple_new("s", strdup("quit")), NULL, NULL);
	}
}


int main() {
	tn_pthread_setsig();
	tn_node *n = tn_node_new();
	t_assert(n != NULL);

	// Create session a
	pthread_t th;
	sa = tn_session_pthread(n, &th);
	t_assert(sa != NULL);

	// Register "quit" and "alpha" patterns
	tn_session_register(sa, tn_tuple_new("s", strdup("quit")), 0, recv_quit, NULL);
	reg_alpha = tn_session_register(sa, tn_tuple_new("s", strdup("alpha")), 0, recv_alpha, (void *)1);

	// Create session b
	pthread_t thb;
	sb = tn_session_pthread(n, &thb);
	t_assert(sb != NULL);

	// Register "alpha" pattern (with willReply = 1)
	tn_session_register(sb, tn_tuple_new("s", strdup("alpha")), 1, recv_alpha_sb, (void *)2);

	// Send start message (which wants a reply)
	tn_session_send(sa, tn_tuple_new("si", strdup("alpha"), (uint64_t)0), recv_reply, (void *)3);

	pthread_join(th, NULL);
	pthread_join(thb, NULL);
	tn_node_unref(n);

	puts("# done");
	t_assert(r_alpha_sb == 1);
	t_assert(r_alpha == 5);
	t_assert(r_quit == 2);
	t_assert(r_reply == 3);
	printf("1..%d\n", test_num);
	return 0;
}

// vim:noet:sw=4:ts=4
