/*
  Copyright (c) 2012 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tanja.h>


static int test_num = 0;

static void test_tuple(const char *in, int inlen, const char *exp, int explen) {
	test_num++;
	int rd = 0;
	tn_tuple *u = tn_json_parse(in, inlen, &rd);
	char *f = NULL;
	if(u) {
		f = tn_json_fmt(u);
		if(!f) {
			printf("not ok %d - (format error)\n   IN: %3d: %s\n  OUT: %3d: ???\n",
				test_num, inlen, in, rd);
		}
		tn_tuple_unref(u);
		if(!f)
			return;
	}
	if(explen == rd && ((!exp && !f) || (exp && f && strcmp(exp, f) == 0)))
		printf("ok %d - (%scorrect JSON)\n   IN: %3d: %s\n  OUT: %3d: %s\n",
			test_num, exp?"":"in", inlen, in, rd, f?f:"(null)");
	else
		printf("not ok %d - (wrong result)\n   IN: %3d: %s\n  EXP: %3d: %s\n  OUT: %3d: %s\n",
			test_num, inlen, in, explen, exp?exp:"(null)", rd, f?f:"(null)");
	if(f)
		free(f);
}


static void test_tuples() {
#define t(x, y) test_tuple(x, strlen(x), y, strlen(x))
#define f(x, y) test_tuple(x, strlen(x), NULL, y)
#define e(x) t(x, x)
	// TODO: test that anything after the JSON string is ignored
	e("[]");
	f("[", 0);
	f("[[[]]", 5);
	f("[inv]", 1);
	f("[-]", 2);
	f("[,]", 1);
	e("[null]");
	t("[true]", "[1]");
	t("[false]", "[0]");
	e("[1]");
	f("[1.]", 3);
	f("[.1]", 1);
	f("[+1]", 1);
	f("[00]", 2);
	f("[--3]", 2);
	t("[1.0]", "[1]");
	e("[0.23]");
	f("[00.23]", 2);
	e("[1.2]");
	e("[-1.5]");
	f("[1.-2]", 3);
	f("[1.+1]", 3);
	f("[1..2]", 3);
	f("[1.1.2]", 4);
	t("[10e-1]", "[1]");
	t("[10E-1]", "[1]");
	t("[10e+1]", "[100]");
	f("[10e--1]", 5);
	f("[10e++1]", 5);
	f("[1e1.4]", 4);
	t("[10e10]", "[100000000000]"); // these kinda depend on what snprintf() does...
	t("[10e0010]", "[100000000000]");
	t("[10.001e0010]", "[100010000000]");
	e("[-9223372036854775808]");
	t("[123456789012345678901234567890]", "[1.23456789012346e+29]");
	t("[-123456789012345678901234567890]", "[-1.23456789012346e+29]");
	t("[0.123456789012345678901234567890]", "[0.123456789012346]");
	t("[-0.123456789012345678901234567890]", "[-0.123456789012346]");
	t("[123456789012345678901234567890.123456789012345678901234567890]", "[1.23456789012346e+29]");
	t("[-123456789012345678901234567890.123456789012345678901234567890]", "[-1.23456789012346e+29]");
	t("[1e-9223372036854775808]", "[0]");
	f("[1e-92233720368547758080]", 23); // doesn't matter much what this does, as long as it doesn't crash/leak
	f("[1e+123456]", 10); // same
	f("[1e+9223372036854775807]", 23); // same
	f("[1e+92233720368547758080]", 24); // same
	t("[0.0e0]", "[0]");
	t("[1.5e1]", "[15]");
	t("[  null   ,\t0 ]", "[null,0]");
	f("[null,]", 6);
	f("[,null]", 1);
	e("[\"a\"]");
	e("[\"a¿月\"]");
	t("[\"\\u0061\\u00BF\\u6708\"]", "[\"a¿月\"]");
	t("[\"\\\\ \\t \\b \\r \\n \\f \\/ '\\\"\"]", "[\"\\\\ \\t \\b \\r \\n \\f / '\\\"\"]");
	f("[\"\t\"]", 2);
	f("[\"\\a\"]", 3);
	f("[\"\\u123\"]", 3);
	f("[\"\\u00G0\"]", 3);
	e("[[[]]]");
	t("[  [ 1 ] ]", "[[1]]");
	e("[{}]");
	t("[ {  }  ]", "[{}]");
	t("[{\"\":true}]", "[{\"\":1}]");
	e("[{\"a\":null,\"b\":\"str\"}]");
	t("[{\"1\" : \"1\"  ,  \"\\t\" \t :  {} } ]", "[{\"1\":\"1\",\"\\t\":{}}]"); // order depends on hash function
	f("[{1:2}]", 2);
	f("[{\"1\"}]", 5);
	f("[{\"\":}]", 5);
	f("[{,\"2\":2}]", 2);
	f("[{\"2\":2,}]", 8);
	e("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]"); // 49
	f("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]", 49); // 50
#undef t
#undef f
#undef e
}


int main() {
	test_tuples();
	printf("1..%d\n", test_num);
	return 0;
}

// vim:noet:sw=4:ts=4
